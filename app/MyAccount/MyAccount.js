'use strict';

angular
    .module('myApp')
    .component('myAccount', {
        templateUrl: 'MyAccount/MyAccount.html'
    })