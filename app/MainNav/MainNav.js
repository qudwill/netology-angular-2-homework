'use strict';

angular
    .module('myApp')
    .component('mainNav', {
        templateUrl: 'MainNav/MainNav.html'
    })