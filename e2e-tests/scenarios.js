'use strict';

const getRandomInt = max => {
  return Math.floor(Math.random() * Math.floor(max));
}

const pokemonsData = require('../app/data/pokemons');

describe('My App', () => {

  /**
   ** Протестировать подсветку текущего пункта меню
   */

  it('Ссылки в меню при клике подсвечиваются корректно', () => {
    browser.get(browser.baseUrl);

    const buttons = element.all(by.css('.btn-default'));

    buttons.each(button => {
      button.click();
      browser.sleep(1000);

      expect(button.getAttribute('class')).toContain('btn-primary');
    });
  });


  /**
   ** Протестировать адрес /myaccount, убедиться что по нему открывается форма.
   */

  it('/#!/myaccount показывает форму', () => {
    browser.get(`${browser.baseUrl}/#!/myaccount`);

    expect(element(by.name('$ctrl.loginForm')).isPresent()).toBe(true);
  });


  /**
   ** Протестировать обязательные поля в форме.
   */

  it('обязательные поля должны иметь класс ng-invalid-required', () => {
    browser.get(`${browser.baseUrl}/#!/myaccount`);

    const form = element(by.name('$ctrl.loginForm'));
    const requiredFields = element.all(by.css('input[required]'));

    requiredFields.each(field => {
      expect(field.getAttribute('class')).toContain('ng-invalid-required');
    });
  });


  /**
   ** Протестировать добавление покемона в корзину.
   */

  it('покемон появляется в корзине', () => {
    browser.get(`${browser.baseUrl}/#!/list`);

    const button = element(by.css('button[ng-click]'));

    button.click();
    browser.sleep(1000);

    expect(element.all(by.css('shopping-cart-component .list-group-item')).count()).toBe(1);
  });


  /**
   ** Протестировать список покемонов. Убедиться что показано столько покемонов, сколько требуется.
   */

  it('список показывает необходимое число покемонов', () => {
    browser.get(`${browser.baseUrl}/#!/list`);

    const UIPokemons = element.all(by.css('ui-view .list-group-item'));
    
    expect(UIPokemons.count()).toBe(Object.keys(pokemonsData).length);
  });
});